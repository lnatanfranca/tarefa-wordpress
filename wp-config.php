<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://wordpress.org/support/article/editing-wp-config-php/
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define( 'DB_NAME', 'local' );

/** MySQL database username */
define( 'DB_USER', 'root' );

/** MySQL database password */
define( 'DB_PASSWORD', 'root' );

/** MySQL hostname */
define( 'DB_HOST', 'localhost' );

/** Database Charset to use in creating database tables. */
define( 'DB_CHARSET', 'utf8' );

/** The Database Collate type. Don't change this if in doubt. */
define( 'DB_COLLATE', '' );

/**
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         'uy/IH0MDmPhxovPMkVx/1GGX8+zIt8kC8sG2BNpppdDncocUZ2yY4LFqlFhcVuGr/aWAGVziZyXFq4w+t2VieQ==');
define('SECURE_AUTH_KEY',  'ywu3x00+GZ9eTyTyC62GkB0w/ROY3LORan0g0CFQUyVFCMwjO/2Kd8JH3Zk9iZ8kYApdiWha0PjdAwP1dAGLUA==');
define('LOGGED_IN_KEY',    'EWnz+7CEA4kgaHJ+LiBNBP5esMaIMYVE3Z61mzshCeYAyJ1ERol7k439TjZD78MbM7VbpAAUW+Zn80pRwsdQWQ==');
define('NONCE_KEY',        'Fjmv15MrvXIW57CK1KDRzUjKAxCMQTRibES7OFUEe2DQhs3RfPxl3fZRGwj3vxkI+U8+jnJmFTV0OhqMualxIQ==');
define('AUTH_SALT',        'tIoYOiIqxtCRxnJuPxsSjb6oRhgXxtkM742aIGhfAx6tpdDWtotTaTba/IQSgy6iT04KJJqLdVb1IxxfjxLs7Q==');
define('SECURE_AUTH_SALT', 'ZhpHGiA3qrezaKGnieW/cPWf6SzkF8wJjNFA5VxeEB3n9QTgVvXMizDUmIB+U0GVPdIiWhuyS2zr99khfRnyRQ==');
define('LOGGED_IN_SALT',   '+jSzCmeuG3hBx6YZrEgsUMk6zaVF1ot9EUWd7fV6XWxcrIy3giqIwelM3k8PB95EqxO++tEEzM9WH3AKJnkCOg==');
define('NONCE_SALT',       'dFbWuY9CVPCtd50RRVq5gh+Xuq5dWnwgsWxTcnG5+x65SWwOZgtOzvg5N9Kkp25diXgrCR330BzweZtpTOhqIA==');

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix = 'wp_';




/* That's all, stop editing! Happy publishing. */

/** Absolute path to the WordPress directory. */
if ( ! defined( 'ABSPATH' ) ) {
	define( 'ABSPATH', dirname( __FILE__ ) . '/' );
}

/** Sets up WordPress vars and included files. */
require_once ABSPATH . 'wp-settings.php';
