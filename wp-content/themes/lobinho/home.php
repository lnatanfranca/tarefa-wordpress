<?php
// Template Name: home
?>

<?php get_header(); ?>

    <main class="main-lista-lobinhos">
        <section class="search-adopted">
            <div class="search">
                <form action="" class="search-form">
                    <input class="search-button" type="button" value="search" onclick="filtraLobos()">
                    <input class="search-name" type="text">
                </form>
                <input class="add-lobo" type="button" value="+ Lobo">
            </div>
            <div class="adopted">
                <input id="lobos-adotados" type="checkbox">
                <label for="lobos-adotados">Ver lobinhos adotados</label>
            </div>
        </section>
        <section class="lista-lobos">
            <?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>
            <div class="perfil-lobo">
                <img class="img-lobo" src="<?php the_field('img_lobo'); ?>" alt="uma foto do(a) lobo(a) ${items[i].name}">
                <div class="texto-lobo">
                    <div class="header-lobo">
                        <div class="nome-idade">
                            <h2><?php the_field('nome_do_lobo'); ?></h2>
                            <p>Idade: <?php the_field('idade'); ?></p>
                        </div>
                        <form class="form-adotar" action="showLobinho.html" method="GET">
                            <input type="hidden" name="adotar" value="">
                            <input class="adotar-btn" type="submit" value="Adotar">
                        </form>
                    </div>
                    <div class="descricao-lobo">
                        <p><?php the_field('descricao');?></p>
                    </div>
                </div>
            </div>
            <?php endwhile; else: ?>
                <p>desculpe, o post não segue os critérios escolhidos</p>
            <?php endif; ?>
    </section>
    </main>

<?php my_pagination(); ?>

<?php get_footer(); ?>