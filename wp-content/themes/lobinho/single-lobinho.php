<?php
// Template Name: single-lobinho
?>

<?php get_header(); ?>
        <main class="main-show-lobinho">
            <div class="img-descricao">
                <div class="img-botoes">
                    <img class="img-lobo" src="<?php echo get_stylesheet_directory_uri() ?>/img/placeholderWolf.png" alt="uma foto do(a) lobo(a) ${lobinho.name}">
                    <div class="lobo-btns">
                        <form action="adotarLobinho.html" method="GET">
                            <input type="hidden" name="adotar" value="${lobinho.id}">
                            <input class="adotar-btn" type="submit" value="ADOTAR">
                        </form>
                        <form action="index.html">                    
                            <input class="excluir-btn" type="submit" onclick="excluirLobinho(${lobinho.id})" value="EXCLUIR">
                        </form>
                    </div>
                </div>
                <div class="descricao-lobo-single">
                    <p>Lorem ipsum dolor, sit amet consectetur adipisicing elit. 
                    Nobis nulla facere maiores molestiae! Facere maxime reprehenderit eligendi laborum, 
                    labore architecto consequuntur sit dolorem necessitatibus a vero ipsam atque exercitationem natus!</p>
                </div>
            </div>
        </main>
<?php get_footer(); ?>