<?php
// Template Name: index
?>

<?php get_header(); ?>

        <main class="main-home">
            <div class="container-adote">
                <h1 class="title">Adote um Lobinho</h1>
                <div class="bar-adote"> </div>
                <p class="description-title">É claro que o consenso sobre a necessidade de qualificação
                   apresenta tendências no sentido de aprovar a manutenção 
                   das regras de conduta normativas.
                </p>
            </div>
            
            <div class="container-sobre">
                <h2>
                    Sobre
                </h2>
                <p class="about">
                    Não obstante, o surgimento do comércio virtual faz parte de um 
                    processo de gerenciamento do levantamento das variáveis envolvidas. 
                    Não obstante, o surgimento do comércio virtual faz parte de um processo 
                    de gerenciamento do levantamento das variáveis envolvidas.Não obstante, 
                    o surgimento do comércio virtual faz parte de um processo de gerenciamento
                    do levantamento das variáveis envolvidas.Não obstante, 
                    o surgimento do comércio virtual faz parte de um processo 
                    de gerenciamento do levantamento das variáveis envolvidas.
                </p>
            </div>
            <div class="container-valores">
                <h2>Valores</h2>
                <ul class="values">
                    <li class="value">
                        <img class="img-value"src = "<?php echo get_stylesheet_directory_uri() ?>/img/lifeInsurance.png" alt="Duas mãos segurando um escudo">
                        <h3>Proteção</h3>
                        <p class="value-description">Assim mesmo, o desenvolvimento contínuo
                            de distintas formas de atuação facilita
                            a criação do sistema de participação geral.
                        </p>
                    </li>
                    <li class="value">
                        <img class="img-value"src = "<?php echo get_stylesheet_directory_uri() ?>/img/care.png" alt="Duas mãos segurando um coração">
                        <h3>Carinho</h3>
                        <p class="value-description">Assim mesmo, o desenvolvimento contínuo
                            de distintas formas de atuação facilita
                            a criação do sistema de participação geral.
                        </p>
                    </li>
                    <li class="value">
                        <img class="img-value"src = "<?php echo get_stylesheet_directory_uri() ?>/img/companionship.png" alt="Uma mão humana e uma pata se tocam">
                        <h3>Companheirismo</h3>
                        <p class="value-description">Assim mesmo, o desenvolvimento contínuo
                            de distintas formas de atuação facilita
                            a criação do sistema de participação geral.
                        </p>
                    </li>
                    <li class="value">
                        <img class="img-value"src = "<?php echo get_stylesheet_directory_uri() ?>/img/rescueDog.png" alt="Um cão de resgate">
                        <h3>Resgate</h3>
                        <p class="value-description">Assim mesmo, o desenvolvimento contínuo
                            de distintas formas de atuação facilita
                            a criação do sistema de participação geral.
                        </p>
                    </li>
                </ul>
                <!--Adicionar os valores-->
            </div>
            <div class="container-lobos-exemplo">
                <h2>Lobos Exemplo</h2>
                <div class="example-wolf" id="wolf-1">
                    <img class="wolf-image" src="<?php echo get_stylesheet_directory_uri() ?>/img/placeholderWolf.png" alt="Imagem de um lobo"/>
                    <div class="wolf-info">
                        <p class="wolf-name">Nome do Lobo</p>
                        <p class="wolf-age">Idade: XX anos</p>
                        <p class="wolf-description">Não obstante, o surgimento do comércio virtual
                             faz parte de um processo de gerenciamento do levantamento 
                             das variáveis envolvidas. Não obstante, o surgimento do comércio
                              virtual faz parte de um processo de gerenciamento do levantamento
                            das variáveis envolvidas.Não obstante, o surgimento do comércio 
                            virtual faz parte de um processo de gerenciamento do levantamento 
                            das variáveis envolvidas.Não obstante, o surgimento do comércio 
                            virtual faz parte de um processo de gerenciamento do
                             levantamento das variáveis envolvidas.</p>
                    </div>
                </div>
                <div class="example-wolf" id="wolf-2">
                    <img class="wolf-image" src="<?php echo get_stylesheet_directory_uri() ?>/img/placeholderWolf.png" alt="Imagem de um lobo"/>
                    <div class="wolf-info">
                        <p class="wolf-name">Nome do Lobo</p>
                        <p class="wolf-age">Idade: XX anos</p>
                        <p class="wolf-description">Não obstante, o surgimento do comércio virtual
                             faz parte de um processo de gerenciamento do levantamento 
                             das variáveis envolvidas. Não obstante, o surgimento do comércio
                              virtual faz parte de um processo de gerenciamento do levantamento
                            das variáveis envolvidas.Não obstante, o surgimento do comércio 
                            virtual faz parte de um processo de gerenciamento do levantamento 
                            das variáveis envolvidas.Não obstante, o surgimento do comércio 
                            virtual faz parte de um processo de gerenciamento do
                             levantamento das variáveis envolvidas.</p>
                    </div>
                </div>
                <!--Os lobos exemplo devem ser buscados pela API-->
            </div>
            <script src="js/lobosExemplo.js"></script>
            
        </main>
<?php get_footer(); ?>