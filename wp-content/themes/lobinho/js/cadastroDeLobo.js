const url = "http://lobinhos.herokuapp.com";
const form = document.querySelector(".adopt-form");


form.addEventListener("submit", (event)=>{    
    const idade = parseInt(form.elements['idade'].value.trim())
    if(isNaN(idade) || idade < 0 || idade > 100){
        //Por algum motivo esse blco não funciona
        alert("O campo 'idade' deve conter um inteiro positivo entre 0-100.");
        event.preventDefault();
        return;
    } 

    if(form.elements['description'].value.length < 10 || form.elements['description'].value.length > 255){
        alert("O campo 'descrição' deve ter entre 10-255 caracteres!");
        event.preventDefault();
        return;
    }

    const body = {
        name: form.elements['name'].value,
        age: idade,
        image_url: form.elements['imgLink'].value,
        description: form.elements['description'].value
    }

    const config = {
        method: "POST",
        body: JSON.stringify(body),
        headers: {"Content-type": "application/json"},
    }

    fetch(url + "/wolves", config)
    .then(response => {
        if(response.ok){
            alert("Success!");
        } else{
            alert(response.status);
        }
    })
    .catch(err => {
        alert("Error: " + err.message);
    })

    alert("");//Esse alert permite que a gente veja os outros alerts antes da página recarregar
    window.location.href = "../index.html";
})