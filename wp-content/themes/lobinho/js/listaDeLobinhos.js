const url = "https://lobinhos.herokuapp.com";

const listaLobos = document.querySelector('.lista-lobos');
const navPaginas = document.querySelector('.nav-paginas');
const buscaNome = document.querySelector('.search-form');
const buscaCampo = document.querySelector('.search-name');
const checkAdopted = document.querySelector('#lobos-adotados');

// função para filtrar adotados 

checkAdopted.addEventListener('click', function(){
    if (checkAdopted.checked){
        showLobinhosAdotados(1);
    } else {
        showLobinhos(1);
    }
})

// função para mostrar a lista de lobos por página

const mostraLista = function(items, pageActual, limitItems){
    let totalPage = Math.ceil(items.length/limitItems);
    let count = (pageActual * limitItems) - limitItems;
    let delimiter = count + limitItems;

    listaLobos.innerHTML = "";

    if (pageActual <= totalPage){
        for (let i = count; i < delimiter; i++){
            let btnString = '';

            if (items[i].adopted) {
                btnString = `
                <input class="adotado-btn" type="button" value="Adotado">
                `
            } else {
                btnString = `<form class="form-adotar" action="showLobinho.html" method="GET">
                    <input type="hidden" name="adotar" value="${items[i].id}">
                    <input class="adotar-btn" type="submit" value="Adotar">
                </form>`
            }

            const div = document.createElement("div");
            div.classList.add("perfil-lobo");

            div.innerHTML = `
                <img class="img-lobo" src="${items[i].image_url}" alt="uma foto do(a) lobo(a) ${items[i].name}">
                <div class="texto-lobo">
                    <div class="header-lobo">
                        <div class="nome-idade">
                            <h2>${items[i].name}</h2>
                            <p>Idade: ${items[i].age} anos</p>
                        </div>
                        ${btnString}
                    </div>
                    <div class="descricao-lobo">
                        <p>${items[i].description}</p>
                    </div>
                </div>
            `;

            listaLobos.appendChild(div);

        }

        // forma o menu de navegação das páginas

        let ceilMenu;
        if (items.length <= 10) {
            navPaginas.innerHTML = "";
        } else if ((totalPage - pageActual) < 5) {
            ceilMenu = totalPage;
            navPaginas.innerHTML = "";
            navegaPagina(pageActual, ceilMenu, totalPage);
        } else {
            ceilMenu = pageActual + 5;
            navPaginas.innerHTML = "";
            navegaPagina(pageActual, ceilMenu, totalPage);
        }

    }
}

// função para filtrar os lobos por nomet

const filtraLobos = function(){
    const termo = buscaNome.children[1].value;
    const termoLower = termo.trim().toLowerCase();
    const termoBusca = termoLower.charAt(0).toUpperCase() + termoLower.slice(1);
    showLobinhosByName(1, termoBusca);
}

// função de get dos lobos

const showLobinhos = function(pagina){
    fetch(url + "/wolves")
    .then((response) => response.json())
    .then((lobinhos) => {
        mostraLista(lobinhos, pagina, 10);
    }).catch((err) => console.error(err));
}

// função de get dos lobos com nome x

const showLobinhosByName = function(pagina, nome){
    fetch(url + "/wolves")
    .then((response) => response.json())
    .then((lobinhos) => {
        let lobinhosPorNome = lobinhos.filter((lobinho) =>
            lobinho.name === nome
        )
        mostraLista(lobinhosPorNome, pagina, lobinhosPorNome.length);
    }).catch((err) => console.error(err));
}

// função de get dos lobos adotados com

const showLobinhosAdotados = function(pagina){
    fetch(url + "/wolves/adopted")
    .then((response) => response.json())
    .then((lobinhos) => {
        mostraLista(lobinhos, pagina, 10);
    }).catch((err) => console.error(err));
}

// função para montar o menu de navegação das páginas

const navegaPagina = function(pagina, totPag){
    const ultimaPag = totPag - 1;
    const ul = document.createElement('ul');
    ul.classList.add('nav-paginas-ul');

    let menu = [];

    if (pagina < 5){
        for (let i = 1; i <= 5; i++) {
            const li = `<li class="pag" onclick="showLobinhos(${i})">${i}</li>`;
            menu.push(li);
        }
    } else if (pagina === ultimaPag) {
        for (let i = ultimaPag - 4; i <= ultimaPag; i++){
            const li = `<li class="pag" onclick="showLobinhos(${i})">${i}</li>`;
            menu.push(li);
        }
    } else {
        for (let i = pagina - 4; i <= pagina + 1; i++) {
            const li = `<li class="pag" onclick="showLobinhos(${i})">${i}</li>`;
            menu.push(li);
        }
    }

    ul.innerHTML = menu.join('');
    navPaginas.appendChild(ul);
}

showLobinhos(1);