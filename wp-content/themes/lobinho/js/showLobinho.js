url = "https://lobinhos.herokuapp.com";

// elementos

const main = document.querySelector('.main');

// pega o id da url

const strParam = (String(window.location.href)).split('?')[1];
const idLobinho = strParam.split('=')[1];

// função para pegar o lobinho de acordo com o id

const showLobinho = function(){
    fetch(url + "/wolves/" + idLobinho)
    .then((response) => response.json())
    .then((lobinho) => {
        mostraLobinho(lobinho);
    }).catch((err) => console.error(err));
}

// função para mostrar o lobinho na página

const mostraLobinho = function(lobinho){
    main.innerHTML = "";

    const div = document.createElement('div');
    div.classList.add('perfil-lobo');

    div.innerHTML = `
        <h1>${lobinho.name}</h1>
        <div class="img-descricao">
            <div class="img-botoes">
                <img class="img-lobo" src="${lobinho.image_url}" alt="uma foto do(a) lobo(a) ${lobinho.name}">
                <div class="lobo-btns">
                    <form action="adotarLobinho.html" method="GET">
                        <input type="hidden" name="adotar" value="${lobinho.id}">
                        <input class="adotar-btn" type="submit" value="ADOTAR">
                    </form>
                    <form action="index.html">                    
                        <input class="excluir-btn" type="submit" onclick="excluirLobinho(${lobinho.id})" value="EXCLUIR">
                    </form>
                </div>
            </div>
            <div class="descricao-lobo">
                <p>${lobinho.description}</p>
            </div>
        </div>
    `

    main.appendChild(div);

}

// função para deletar lobo 

const excluirLobinho = function(id){
    const config = {
        method: "DELETE"
    }

    fetch(url + "/wolves/" + id, config)
    .catch((err) => console.error(err));

    alert("Lobinho foi excluído.")
}

// inicializa a pagina

showLobinho();