<footer class="container-footer">
            <iframe class="iframe-IC" style="border:0" loading="lazy" src="https://www.google.com/maps/embed/v1/place?q=place_id:ChIJK2lORH6BmQAR9fJ6V_s1Xv0&key=AIzaSyBjqMWIiZ_yM8GRPzFPSzIdcqjHcV6RbtE"></iframe>
            <div class="middle-footer">
                <ul>
                    <li class="location">Av. Milton Tavares de Souza, s/n - Sala 115 B - Boa Viagem, Niterói - RJ, 24210-315</li>
                    <li class="phone">(99) 99999-9999</li>
                    <li class="email">salve-lobos@lobINhos.com</li>
                </ul>
                <button class="button-who" onclick="window.location.href = 'quemSomos.html'">Quem Somos</button>
            </div>
            <div class="end-footer">
                <p>
                    Desenvolvido com
                </p>
                <img src="<?php echo get_stylesheet_directory_uri() ?>/img/logoPata.png" alt="Imagem de uma pata com formato de coração. Um lado tem a cor amarela e o outro azul">
            </div>      
        </footer>
    </div>
    <?php wp_footer(); ?>
</body>
</html>